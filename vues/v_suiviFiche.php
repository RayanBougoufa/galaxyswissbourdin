
<div class="panel panel-primary_c">
    <div class="panel-heading"><strong>Listes des fiches</strong></div>
    <table class="table table-bordered table-responsive">
        <theader>
            <tr>
                <th>Nom <span style="float:right;">
                    <ul class="sort-icons">
                        <li>
                            <a class="sort-asc"
                               href="index.php?uc=suiviFiche&action=listeFiches&order=nom&sort=ASC">
                                <span class="glyphicon glyphicon-triangle-top"></span>
                            </a>
                        </li>
                        <li>
                            <a class="sort-desc" href="index.php?uc=suiviFiche&action=listeFiches&order=nom&sort=DESC">
                                <span class="glyphicon glyphicon-triangle-bottom"></span>
                            </a>
                        </li>
                    </ul>
                </th>
                <th>Prenom
                    <span style="float:right;">
                    <ul class="sort-icons">
                        <li>
                            <a class="sort-asc"
                               href="index.php?uc=suiviFiche&action=listeFiches&order=prenom&sort=ASC">
                                <span class="glyphicon glyphicon-triangle-top"></span>
                            </a>
                        </li>
                        <li>
                            <a class="sort-desc" href="index.php?uc=suiviFiche&action=listeFiches&order=prenom&sort=DESC">
                                <span class="glyphicon glyphicon-triangle-bottom"></span>
                            </a>
                        </li>
                    </ul>
                </th>
                <th>Mois<span style="float:right;">
                   <ul class="sort-icons">
                        <li>
                            <a class="sort-asc"
                               href="index.php?uc=suiviFiche&action=listeFiches&order=mois&sort=ASC">
                                <span class="glyphicon glyphicon-triangle-top"></span>
                            </a>
                        </li>
                        <li>
                            <a class="sort-desc" href="index.php?uc=suiviFiche&action=listeFiches&order=mois&sort=DESC">
                                <span class="glyphicon glyphicon-triangle-bottom"></span>
                            </a>
                        </li>
                    </ul>
                </th>
                <th>Montant Validé(€)<span style="float:right;">
                    <ul class="sort-icons">
                        <li>
                            <a class="sort-asc"
                               href="index.php?uc=suiviFiche&action=listeFiches&order=montantValide&sort=ASC">
                                <span class="glyphicon glyphicon-triangle-top"></span>
                            </a>
                        </li>
                        <li>
                            <a class="sort-desc" href="index.php?uc=suiviFiche&action=listeFiches&order=montantValide&sort=DESC">
                                <span class="glyphicon glyphicon-triangle-bottom"></span>
                            </a>
                        </li>
                    </ul>
                </th>
                <th>Date Modification<span style="float:right;">
                    <ul class="sort-icons">
                        <li>
                            <a class="sort-asc"
                               href="index.php?uc=suiviFiche&action=listeFiches&order=dateModif&sort=ASC">
                                <span class="glyphicon glyphicon-triangle-top"></span>
                            </a>
                        </li>
                        <li>
                            <a class="sort-desc" href="index.php?uc=suiviFiche&action=listeFiches&order=dateModif&sort=DESC">
                                <span class="glyphicon glyphicon-triangle-bottom"></span>
                            </a>
                        </li>
                    </ul>
                </th>
                <th>Etat fiche<span style="float:right;">
                   <ul class="sort-icons">
                        <li>
                            <a class="sort-asc"
                               href="index.php?uc=suiviFiche&action=listeFiches&order=etat&sort=ASC">
                                <span class="glyphicon glyphicon-triangle-top"></span>
                            </a>
                        </li>
                        <li>
                            <a class="sort-desc" href="index.php?uc=suiviFiche&action=listeFiches&order=etat&sort=DESC">
                                <span class="glyphicon glyphicon-triangle-bottom"></span>
                            </a>
                        </li>
                    </ul>
                </th>
                <th></th>
            </tr>
        </theader>
        <tbody class="liste-suivi">
        <?php
        foreach ($listeFiches as $uneFiche) {
            $id = $uneFiche['idVisiteur'];
            $nom = htmlspecialchars($uneFiche['nom']);
            $prenom = htmlspecialchars($uneFiche['prenom']);
            $mois = ($uneFiche['mois']);
            $numAnnee = substr($mois, 0, 4);
            $numMois = substr($mois, 4, 2);
            $montantValide = ($uneFiche['montantValide']);
            $dateModif = ($uneFiche['dateModif']);
            $etat = htmlspecialchars($uneFiche['etat']); ?>
            <tr>
                <td><?php echo $nom ?></td>
                <td><?php echo $prenom ?></td>
                <td><?php echo $numMois . '/' . $numAnnee ?></td>
                <td><?php echo $montantValide ?></td>
                <td><?php echo $dateModif ?></td>
                <td><strong><?php echo $etat ?></strong></td>
                <td align="center">
                    <a href="index.php?uc=suiviFiche&action=infosFiche&id=<?php echo $id?>&mois=<?php echo $mois?>">
                        <span class="glyphicon glyphicon-search"></span>
                    </a>
                </td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
</div>