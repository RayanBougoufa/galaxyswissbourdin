
<hr>
<div class="panel panel-orange">
    <div class="panel-heading">Fiche de frais de
        <strong><?php echo $nom . ' ' . $prenom ?></strong>
        (<?php echo $numMois . '-' . $numAnnee ?>) :  </div>
    <div class="panel-body">
        <strong><u>Etat :</u></strong> <?php echo $libEtat ?>
        depuis le <?php echo $dateModif ?> <br>
        <strong><u>Montant validé :</u></strong> <?php echo $montantValide ?> €
    </div>
</div>
<div class="panel panel-orange">
    <div class="panel-heading orange-clair">Eléments forfaitisés</div>
    <table class="table table-bordered table-responsive">
        <tr>
            <?php
            foreach ($lesFraisForfait as $unFraisForfait) {
                $libelle = $unFraisForfait['libelle']; ?>
                <th> <?php echo htmlspecialchars($libelle) ?></th>
                <?php
            }
            ?>
        </tr>
        <tr>
            <?php
            foreach ($lesFraisForfait as $unFraisForfait) {
                $quantite = $unFraisForfait['quantite']; ?>
                <td class="qteForfait"><?php echo $quantite ?> </td>
                <?php
            }
            ?>
        </tr>
    </table>
</div>
<div class="panel panel-orange">
    <div class="panel-heading orange-clair">Descriptif des éléments hors forfait -
        <?php echo $nbJustificatifs ?> justificatifs reçus</div>
    <table class="table table-bordered table-responsive">
        <tr>
            <th class="date">Date</th>
            <th class="libelle">Libellé</th>
            <th class='montant'>Montant</th>
        </tr>
        <?php
        foreach ($lesFraisHorsForfait as $unFraisHorsForfait) {
            $date = $unFraisHorsForfait['date'];
            $libelle = htmlspecialchars($unFraisHorsForfait['libelle']);
            $montant = $unFraisHorsForfait['montant']; ?>
            <tr>
                <td><?php echo $date ?></td>
                <td><?php echo $libelle ?></td>
                <td><?php echo $montant ?></td>
            </tr>
            <?php
        }
        ?>
    </table>
</div>
<?php
    if ($idEtat ===  'VA') {
        ?>
        <div>
            <form method="post"
                  action="index.php?uc=suiviFiche&action=mettreEnPaiment"
                  id="mettre-en-paiment"
                  role="form">
                <input class="form-control" name="idVisiteur" type="hidden" value="<?php echo $idVisiteur ?>">
                <input class="form-control" name="mois" type="hidden" value="<?php echo $mois ?>">
                <button class=" btn btn-success">Mettre en paiment</button>
            </form>
        </div>
    <?php
    }
    if ($idEtat ===  'MP') {
    ?>
        <div>
            <form method="post"
                  action="index.php?uc=suiviFiche&action=fichePayee"
                  id="fiche-payé"
                  role="form">
                <input class="form-control" name="idVisiteur" type="hidden" value="<?php echo $idVisiteur ?>">
                <input class="form-control" name="mois" type="hidden" value="<?php echo $mois ?>">
                <button class=" btn btn-success">Ajouter aux fiches payées</button>
            </form>
        </div>
    <?php
    }
    ?>
<br>