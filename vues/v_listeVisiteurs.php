
<div class="col-md-4">
    <h3>Sélectionner un visiteur : </h3>
</div>
<div class="col-md-4">
    <form action="index.php?uc=valideFrais&action=listeMois" method="post" action="" role="form">
        <div class="form-group">
            <label for="lstMois" accesskey="n">Visiteur : </label>
            <select id="idvisiteur" name="idvisiteur" class="form-control selectpicker" data-live-search="true">

                <?php
                foreach ($lesvisiteurs as $unVisiteur) {
                    $id = $unVisiteur['id'];
                    $nom = $unVisiteur['nom'];
                    $prenom = $unVisiteur['prenom'];
                 ?>
                        <option value="<?php echo $id ?>">
                            <?php echo $nom . '/' . $prenom ?> </option>
                        <?php
                }
                ?>    
            </select>
        </div>
        <input id="ok" type="submit" value="Valider" class="btn btn-success" 
               role="button">
    </form>
</div>       
