<?php
/**
 * Vue État de Frais
 *
 * PHP Version 7
 *
 * @category  PPE
 * @package   GSB
 * @author    Réseau CERTA <contact@reseaucerta.org>
 * @author    José GIL <jgil@ac-nice.fr>
 * @copyright 2017 Réseau CERTA
 * @license   Réseau CERTA
 * @version   GIT: <0>
 * @link      http://www.reseaucerta.org Contexte « Laboratoire GSB »
 */
?>
<hr>

<div>Eléments forfaitisés</div>

<form id="formForfait" method="post" action="index.php?uc=valideFrais&action=majforfait" role="form">
    <fieldset>       
        <?php
        foreach ($lesFraisForfait as $unFrais) {
            $idFrais = $unFrais['idfrais'];
            $libelle = htmlspecialchars($unFrais['libelle']);
            $quantite = $unFrais['quantite'];
            ?>
            <div class="form-group">
                <label for="idFrais"><?php echo $libelle ?></label>
                <input type="text" id="idFrais" 
                       name="lesFrais[<?php echo $idFrais ?>]"
                       size="10" maxlength="5" 
                       value="<?php echo $quantite ?>" 
                       class="form-control">
            </div>
            <?php
        }
        ?>
        <button class="btn btn-success" type="submit">corriger</button>
        <button class="btn btn-danger" type="reset">réinitialiser</button>
    </fieldset>
</form>   



<br>




<div class="panel panel-info">
    <div class="panel-heading">Descriptif des éléments hors forfait - 
<?php echo $nbJustificatifs ?> justificatifs reçus</div>
    <table class="table table-bordered table-responsive">
        <tr>
            <th> </th>
            <th class="date">Date</th>
            <th class="libelle">Libellé</th>
            <th class='montant'>Montant</th>                
        </tr>
        <?php
        foreach ($lesFraisHorsForfait as $unFraisHorsForfait) {
            $date = $unFraisHorsForfait['date'];
            $libelle = htmlspecialchars($unFraisHorsForfait['libelle']);
            $montant = $unFraisHorsForfait['montant'];
            ?>
            <tr>
            <form id="formHorsForfait" method="POST" action="index.php?uc=valideFrais&action=majFraisHorsForfait">
                <td><input type="hidden" id="idFraisHF" name="idFraisHF" value="<?php echo $unFraisHorsForfait['id'] ?>"> </td>

                <td><input type="text" id="dateFraisHF" name="dateFraisHF" value="<?php echo $date ?>"></td>
                <td><input type="text" id="libelleFraisHF" name="libelleFraisHF" value="<?php echo $libelle ?>"></td>
                <td><input  type="text" id="montantFraisHF" name="montantFraisHF" value="<?php echo $montant ?>"></td>

                <td><input  id="corriger" type="submit" name="btn_corriger" value="corriger" class="btn btn-success center-block"></td>
                <td><input  id="reporter"type="submit" name="btn_reporter" value="reporter" class="btn btn-warning center-block"></td>
                <td><input  id="refuser" type="submit" name="btn_refuser" value="refuser" class="btn btn-danger center-block"></td>

            </form>
            </tr>
            <?php
        }
        ?>

    </table>



</div>
<a href=index.php?uc=valideFrais&action=validerFrais> <input id="accepter" type="submit" name="btn_valider" value="accepter la fiche frais" class="btn btn-success center-block"></a>



