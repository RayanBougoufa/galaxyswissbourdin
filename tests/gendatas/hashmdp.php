<?php
require './fonctions.php';
$pdo = new PDO('mysql:host=localhost;dbname=gsb_frais', 'root', '');
$pdo->query('SET CHARACTER SET utf8');

$lesVisiteurs = getLesVisiteurs($pdo);
$lesComptables = getLesComptables($pdo);

/**
 * Nous passons de 20 caractères a 60 (une chaine de caractère bcrypt) 
 * Modification des caractéristiques de la colonne mdp des tables.
 *
 * @return null
 */ 

$requeteprepare = $pdo->prepare(
        'ALTER TABLE comptable'
        . ' MODIFY mdp char(60);'
        );
$requeteprepare->execute();
$requeteprepare = null;

$requeteprepare = $pdo->prepare(
        'ALTER TABLE visiteur'
        . ' MODIFY mdp char(60);'
        );
$requeteprepare->execute();
$requeteprepare = null;



/**
 * Boucles créant un hashage du mot de passe en clair , puis remplace le mot de passe par le mot de passe hashé
 * il existe deux boucles , une pour les visiteurs , l'autre pour les comptables
 *  
 */
foreach($lesVisiteurs as $unVisiteur){
    $motdepassevisiteur = $unVisiteur["mdp"];
    $idVisiteur = $unVisiteur["id"];
    $mdphashé = password_hash($motdepassevisiteur, PASSWORD_DEFAULT);
    $requeteprepare = $pdo->prepare (
            'UPDATE visiteur'
            . ' SET mdp = :mdphash'
            . ' WHERE mdp = :mdp'
            );
    $requeteprepare->bindValue(':mdphash', $mdphashé, PDO::PARAM_STR);
    $requeteprepare->bindValue(':mdp', $motdepassevisiteur, PDO::PARAM_STR);
    $requeteprepare->execute();
    echo  'un Visiteur vient d\'etre modifié' . '<br>';
}

foreach($lesComptables as $unComptable){
    $motdepassecomptable = $unComptable["mdp"];
    $idComptable = $unComptable["id"];
    $mdphashé = password_hash($motdepassecomptable, PASSWORD_DEFAULT);
    $requeteprepare = $pdo->prepare (
            'UPDATE comptable'
            . ' SET mdp = :mdphash'
            . ' WHERE mdp = :mdp'
            );
    $requeteprepare->bindParam(':mdphash', $mdphashé, PDO::PARAM_STR);
    $requeteprepare->bindParam(':mdp', $motdepassecomptable, PDO::PARAM_STR);
    $requeteprepare->execute();
    echo 'un Comptable vient d\'etre modifié' .'</br>';
}
