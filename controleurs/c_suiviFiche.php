<?php

/**
 * Gestion des frais
 *
 * PHP Version 7
 *
 * @category  PPE
 * @package   GSB
 * @author    Réseau CERTA <contact@reseaucerta.org>
 * @author    José GIL <jgil@ac-nice.fr>
 * @copyright 2017 Réseau CERTA
 * @license   Réseau CERTA
 * @version   GIT: <0>
 * @link      http://www.reseaucerta.org Contexte « Laboratoire GSB »
 */
$action = filter_input(INPUT_GET, 'action', FILTER_SANITIZE_STRING);

switch ($action){
    case 'listeFiches':
        $order = filter_input(INPUT_GET, 'order', FILTER_SANITIZE_STRING);
        $sort = filter_input(INPUT_GET, 'sort', FILTER_SANITIZE_STRING);
        $listeFiches = $pdo->getLesFicheFraisApayer($order,$sort);
        include 'vues/v_suiviFiche.php';
        break;
    case 'infosFiche':
        $mois = filter_input(INPUT_GET, 'mois', FILTER_SANITIZE_STRING);
        $idVisiteur = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING);
        $infosVisiteur = $pdo->getInfosVisiteurParId($idVisiteur);
        $nom = $infosVisiteur['nom'];
        $prenom = $infosVisiteur['prenom'];
        $lesFraisHorsForfait = $pdo->getLesFraisHorsForfait($idVisiteur, $mois);
        $lesFraisForfait = $pdo->getLesFraisForfait($idVisiteur, $mois);
        $lesInfosFicheFrais = $pdo->getLesInfosFicheFrais($idVisiteur, $mois);
        $numAnnee = substr($mois, 0, 4);
        $numMois = substr($mois, 4, 2);
        $idEtat = $lesInfosFicheFrais['idEtat'];
        $libEtat = $lesInfosFicheFrais['libEtat'];
        $montantValide = $lesInfosFicheFrais['montantValide'];
        $nbJustificatifs = $lesInfosFicheFrais['nbJustificatifs'];
        $dateModif = dateAnglaisVersFrancais($lesInfosFicheFrais['dateModif']);
        include 'vues/v_infosFiche.php';
        break;
    case 'mettreEnPaiment':
        $mois = filter_input(INPUT_POST, 'mois', FILTER_SANITIZE_STRING);
        $idVisiteur = filter_input(INPUT_POST, 'idVisiteur', FILTER_SANITIZE_STRING);
        $pdo->majEtatFicheFrais($idVisiteur,$mois,'MP');
        if (nbSucces()) {
            include 'vues/v_succes.php';
        }else if (nbErreurs()) {
            include 'vues/v_erreurs.php';
        }
        $listeFiches = $pdo->getLesFicheFraisApayer('dateModif','DESC');
        include 'vues/v_suiviFiche.php';
        break;
    case 'fichePayee':
        $mois = filter_input(INPUT_POST, 'mois', FILTER_SANITIZE_STRING);
        $idVisiteur = filter_input(INPUT_POST, 'idVisiteur', FILTER_SANITIZE_STRING);
        $pdo->majEtatFicheFrais($idVisiteur,$mois,'RB');
        if (nbSucces()) {
            include 'vues/v_succes.php';
        }else if (nbErreurs()) {
            include 'vues/v_erreurs.php';
        }
        $listeFiches = $pdo->getLesFicheFraisApayer('dateModif','DESC');
        include 'vues/v_suiviFiche.php';
        break;
}