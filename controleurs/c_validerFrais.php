<?php

/**
 * Gestion des frais
 *
 * PHP Version 7
 *
 * @category  PPE
 * @package   GSB
 * @author    Réseau CERTA <contact@reseaucerta.org>
 * @author    José GIL <jgil@ac-nice.fr>
 * @copyright 2017 Réseau CERTA
 * @license   Réseau CERTA
 * @version   GIT: <0>
 * @link      http://www.reseaucerta.org Contexte « Laboratoire GSB »
 */
$action = filter_input(INPUT_GET, 'action', FILTER_SANITIZE_STRING);
$lesvisiteurs = $pdo->getLesVisiteurs();




switch ($action) {
    case 'listeVisiteur' :

        include 'vues/v_listeVisiteurs.php';
        break;
    case 'listeMois' :

        $_SESSION ['chxlstvisiteur'] = filter_input(INPUT_POST, 'idvisiteur', FILTER_SANITIZE_STRING);
        $lesMois = $pdo->getLesMoisDisponibles($_SESSION ['chxlstvisiteur']);

        if ($lesMois) {
            include 'vues/v_listeVisiteurs.php';
            include 'vues/v_listeMoiscomptable.php';
        } else {
            include 'vues/v_listeVisiteurs.php';
            echo "le visiteur selectioné n'a aucune fiche de frais";
        }
        break;
    case 'voirFiche' :

        $_SESSION['mois'] = filter_input(INPUT_POST, 'lstMois', FILTER_SANITIZE_STRING);
        include 'vues/v_listeVisiteurs.php';
        include 'vues/v_listeMoiscomptable.php';
        $lesFraisHorsForfait = $pdo->getLesFraisHorsForfait($_SESSION ['chxlstvisiteur'], $_SESSION['mois']);
        $lesFraisForfait = $pdo->getLesFraisForfait($_SESSION ['chxlstvisiteur'], $_SESSION['mois']);
        $lesInfosFicheFrais = $pdo->getLesInfosFicheFrais($_SESSION ['chxlstvisiteur'], $_SESSION['mois']);
        $montantValide = $lesInfosFicheFrais['montantValide'];
        $nbJustificatifs = $lesInfosFicheFrais['nbJustificatifs'];
        $dateModif = dateAnglaisVersFrancais($lesInfosFicheFrais['dateModif']);
        include 'vues/v_validerFrais.php';
        break;

    case 'majforfait' :
        $lesFrais = filter_input(INPUT_POST, 'lesFrais', FILTER_DEFAULT, FILTER_FORCE_ARRAY);
        if (lesQteFraisValides($lesFrais)) {
            $pdo->majFraisForfait($_SESSION ['chxlstvisiteur'], $_SESSION['mois'], $lesFrais);
        } else {
            ajouterErreur('Les valeurs des frais doivent être numériques');
            include 'vues/v_erreurs.php';
        }
        break;


    case 'majFraisHorsForfait':

        $leVisiteur = $_SESSION['chxlstvisiteur'];
        $leMois = $_SESSION['mois'];
        if (isset($_POST['btn_corriger'])) {
            var_dump($_POST['idFraisHF'], $_POST['libelleFraisHF'], $_POST['dateFraisHF'], $_POST['montantFraisHF']);

            $pdo->majFraisHorsForfait($_POST['idFraisHF'], $_POST['libelleFraisHF'], $_POST['dateFraisHF'], $_POST['montantFraisHF']);
        }

        // Refus d'un frais hors forfait
        if (isset($_POST['btn_refuser'])) {
            if (strlen($_POST['libelleFraisHF']) > 44) {
                $_POST['libelleFraisHF'] = substr($_POST['libelleFraisHF'], 0, 44);
                $_POST['libelleFraisHF'] = "REFUSÉ" . $_POST['libelleFraisHF'];
            } else {
                $_POST['libelleFraisHF'] = "REFUSÉ " . $_POST['libelleFraisHF'];
            }

            $pdo->refuseFraisHorsForfait($_POST['idFraisHF'], $_POST['libelleFraisHF']);
        }

        //cas ou on vezut reporter un frais 
        if (isset($_POST['btn_reporter'])) {

            $moisSuivant = strval($leMois + 1);

            //si le visiteur n'as pas de fiche pour le mois suivant on la créer
            if ($pdo->estPremierFraisMois($leVisiteur, $moisSuivant)) {

                $pdo->creeNouvellesLignesFrais($leVisiteur, $moisSuivant);
            }
            $pdo->creeNouveauFraisHorsForfait($leVisiteur, $moisSuivant, $_POST['libelleFraisHF'], $_POST['dateFraisHF'], $_POST['montantFraisHF']);
            $pdo->supprimerFraisHorsForfaitFiche($_POST['idFraisHF'], $leMois);
        }

        $lesFraisHorsForfait = $pdo->getLesFraisHorsForfait($leVisiteur, $leMois);
        $lesFraisForfait = $pdo->getLesFraisForfait($leVisiteur, $leMois);
        $lesInfosFicheFrais = $pdo->getLesInfosFicheFrais($leVisiteur, $leMois);
        $nbJustificatifs = $lesInfosFicheFrais['nbJustificatifs'];
        break;

    case 'validerFrais':
        $pdo->majEtatFicheFrais($_SESSION['chxlstvisiteur'], $_SESSION['mois'], "VA");
        ?>
        <script>
            alert("la fiche a été validée!");
        </script>
        <?php

        header("Location:index.php?uc=valideFrais&action=listeVisiteur"); 
        




        break;
}

