<?php

require 'fpdf/fpdf181/fpdf.php';

class monPDF extends FPDF {

    private $tarifUnitaire = array(
        "ETP" => 110,
        "KM" => 0.62,
        "NUI" => 80,
        "REP" => 25
    );

    function Header() {
        //insertion du logo 
        $this->Image('http://gsb/logo.jpg', 100, 10, 25);
        // choix de la police d'ecriture
        $this->SetFont('helvetica', 'B', 10);
        $this->SetTitle('PDF RECAP DES FRAIS', true);
    }

    function Footer() {
        // Positionnement à 1,5 cm du bas
        $this->SetY(-20);
        // Police Arial italique 8
        $this->SetFont('Arial', 'I', 8);
        // Numéro de page
        $this->Cell(0, 10, 'Page ' . $this->PageNo() . '/{nb}', 0, 0, 'C');
    }

    public function newPDF($tableau) {
        $fullTotal = 0;
        $nomcomplet = $_SESSION["nom"] . " " . $_SESSION["prenom"];
        $lemois = $_SESSION["lemois"];
        $lesinfos = $tableau["lesInfos"];
        $name = $nomcomplet . " " . $lemois;
        $file = $name . ".pdf";
        $chemin = $_SERVER['DOCUMENT_ROOT'] . '/factures/' . $name . ".pdf";
        if (file_exists($chemin)) {
            ob_clean();
            $file = "/factures/" . $file;
            header('Location: ' . $file . ' ');
            exit();
        }
        $lesFrais = $tableau["lesFrais"];
        $lesFraisHF = $tableau["lesFraisHF"];
        $pdf = new monPDF();
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->SetAutoPageBreak(true, 20);
        $pdf->SetFont('helvetica', 'B', 10);
        $pdf->Cell(0, 30, '');
        $pdf->Ln();
        $pdf->Cell(50, 10, "Visiteur : ", 0, "L");
        $pdf->Cell(50, 10, $nomcomplet);
        $pdf->Ln();
        $pdf->Cell(50, 10, "Mois : ", 0, "L");
        $pdf->Cell(50, 10, $lemois);
        $pdf->Ln();
        $pdf->Cell(0, 7, "REMBOURSEMENT DE FRAIS ENGAGES", 1, 1, 'C');
        $pdf->Cell(48, 7, "Frais Forfaitaires", 'LB', 0, 'C');
        $pdf->Cell(48, 7, "Quantite", 'B', 0, 'C');
        $pdf->Cell(47, 7, "Montant unitaire", 'B', 0, 'C');
        $pdf->Cell(47, 7, "Total", 'BR', 0, 'C');
        $pdf->Ln();
        foreach ($lesFrais as $unFrais) {
            $total = (float) $unFrais["quantite"] * (float) $this->tarifUnitaire[$unFrais["idfrais"]];
            $fullTotal += $total;
            $pdf->Cell(48, 7, utf8_decode($unFrais["libelle"]), 1, 0, 'C');
            $pdf->Cell(48, 7, $unFrais["quantite"], 1, 0, 'C');
            $pdf->Cell(47, 7, $this->tarifUnitaire[$unFrais["idfrais"]], 1, 0, 'C');
            $pdf->Cell(47, 7, $total, 1, 0, 'C');
            $pdf->Ln();
        }
        if (isset($lesFraisHF)) {
            $pdf->Ln();
            $pdf->Cell(0, 10, "Autres Frais", 1, 1, 'C');
            $pdf->Cell(63, 7, "Date", 'LB', 0, 'C');
            $pdf->Cell(63, 7, "Libelle", 'B', 0, 'C');
            $pdf->Cell(64, 7, "Montant", 'BR', 0, 'C');
            $pdf->Ln();
            foreach ($lesFraisHF as $unFraisHF) {
                $pdf->Cell(63, 7, $unFraisHF["date"], 1, 0, 'C');
                $pdf->Cell(63, 7, utf8_decode($unFraisHF["libelle"]), 1, 0, 'C');
                $pdf->Cell(64, 7, $unFraisHF["montant"], 1, 0, 'C');
                $pdf->Ln();
                $fullTotal += (int) $unFraisHF["montant"];
            }
        }
        $pdf->Cell(0, 30, "");
        $pdf->Ln();
        $pdf->Cell(100, 10, "");
        $pdf->Cell(50, 10, "TOTAL DU " . $lemois . " :", 1, 0, 'C');
        $pdf->Cell(30, 10, $fullTotal, 1, 0, 'C');
        $pdf->Ln();
        $pdf->Cell(0, 20, "");
        $pdf->Ln();
        $pdf->Cell(100, 10, "");
        date_default_timezone_set('Europe/Paris');
        setlocale(LC_TIME, 'fr_FR.utf8', 'fra'); // OK
        $pdf->Cell(70, 10, "Fait a Paris le " . strftime("%A %d %B %Y"), 0, 0, 'C');
        $pdf->Ln();
        $pdf->Cell(0, 15, "");
        ob_clean();
        $pdf->Ln();
        $pdf->Cell(100, 10, "");
        $pdf->Cell(70, 10, "Vu l'agent comptable.", 0, 0, 'C');
        $pdf->Ln();
        $pdf->Cell(100, 10, "");
        $pdf->Image('http://gsb/SC.jpg', 125, null);
        $chemin = $_SERVER['DOCUMENT_ROOT'] . '/factures/' . $name . ".pdf";
        $pdf->Output('F', $chemin, true);
        $pdf->Output('I', $name, true);
    }

}

?>